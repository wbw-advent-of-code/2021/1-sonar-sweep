package functions

infix fun Int.isGreaterThen(x: Int): Boolean {
    return this > x
}

fun List<Int>.slidingWindowSum(index: Int): Int? {
    return if (index < 2) {
        null
    } else {
        this[index - 2] + this[index - 1] + this[index]
    }
}