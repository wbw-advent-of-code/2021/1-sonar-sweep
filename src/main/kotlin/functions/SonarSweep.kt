package functions

import java.io.File

class SonarSweep(private val fileName: String) {
    fun sweep(): Int {
//        var count = 0;
//
//        val measurements = File(fileName).useLines { it.toMutableList() }
//            .map { it.toInt() }
//
//        for ((index, value) in measurements.withIndex()) {
//            if (index > 0) {
//                if (value isGreaterThen measurements[index - 1]) {
//                    count++
//                }
//            }
//        }
//
//        return count

        return File(fileName).useLines { it.toMutableList() }
            .map { it.toInt() }
            .windowed(2)
            .count { (a,b) -> a < b }
    }

    fun slidingSweep(): Int {
//        var count = 0;
//        val measurements = File(fileName).useLines { it.toMutableList() }
//            .map { it.toInt() }
//        val slidingWindows = mutableListOf<Int>()
//
//        for ((index) in measurements.withIndex()) {
//            measurements.slidingWindowSum(index)
//                ?.let { slidingWindows.add(it) }
//        }
//
//        for ((index, value) in slidingWindows.withIndex()) {
//            if (index > 0) {
//                if (value isGreaterThen slidingWindows[index - 1]) {
//                    count++
//                }
//            }
//        }
//
//        return count

        return File(fileName).useLines { it.toMutableList() }
            .map { it.toInt() }
            .windowed(4)
            .count { (a,b,c,d) -> a < d }
    }
}