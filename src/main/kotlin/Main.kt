import functions.SonarSweep

fun main(args: Array<String>) {
    val file = "src/main/resources/input.txt"

    println(SonarSweep(file).sweep())
    println(SonarSweep(file).slidingSweep())
}