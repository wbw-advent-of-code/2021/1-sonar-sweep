package functions

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.assertEquals


internal class FunctionsKtTest {
    @ParameterizedTest(name = "{0} isGreaterThen {1} returns {2}")
    @MethodSource("igtSource")
    internal fun isGreaterThenReturnsExpected(a: Int, b: Int, expected: Boolean) {
        assertEquals(expected, a isGreaterThen b)
    }

    @ParameterizedTest
    @MethodSource("sumSource")
    internal fun slidingWindowSum(index: Int, expected: Int?) {
        val listOfInts = (1..10).toList()

        assertEquals(expected, listOfInts.slidingWindowSum(index))
    }

    companion object {
        @JvmStatic
        fun igtSource(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(1, 2, false),
                Arguments.of(1, 1, false),
                Arguments.of(2, 1, true)
            )
        }

        @JvmStatic
        fun sumSource(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(0, null),
                Arguments.of(1, null),
                Arguments.of(2, 6),
                Arguments.of(3, 9),
                Arguments.of(4, 12),
                Arguments.of(5, 15),
                Arguments.of(6, 18),
                Arguments.of(7, 21),
                Arguments.of(8, 24),
                Arguments.of(9, 27)
            )
        }
    }
}