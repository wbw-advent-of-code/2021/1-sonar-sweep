import functions.SonarSweep
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class SonarSweepIntegrationTest {
    private val testFile: String = "src/test/resources/test-input.txt"

    @Test
    internal fun `sweep returns expected result`() {
        assertEquals(7, SonarSweep(testFile).sweep())
    }

    @Test
    internal fun slidingWindowSweepReturnsExpected() {
        assertEquals(5, SonarSweep(testFile).slidingSweep())
    }
}